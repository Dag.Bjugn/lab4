package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {

	IGrid currentBrian;
	
	public BriansBrain(int rows, int columns) {
		currentBrian = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}
	
	@Override
	public CellState getCellState(int row, int column) {
		return currentBrian.get(row, column);
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentBrian.numRows(); row++) {
			for (int col = 0; col < currentBrian.numColumns(); col++) {
				if (random.nextInt(3) == 0) {
					currentBrian.set(row, col, CellState.ALIVE);
				} else if(random.nextBoolean()){
					currentBrian.set(row, col, CellState.DYING);
				} else {
					currentBrian.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public void step() {
		IGrid nextBrian = currentBrian.copy();
		for (int r = 0; r < currentBrian.numRows(); r++) {
			for (int c = 0; c < currentBrian.numColumns(); c++) {
				nextBrian.set(r, c, getNextCell(r, c));
			}
		}
		currentBrian = nextBrian;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		
		if(getCellState(row, col) == CellState.ALIVE)
			return CellState.DYING;
		else if (getCellState(row, col) == CellState.DYING)
			return CellState.DEAD;
		if(countNeighbors(row, col) == 2)
			return CellState.ALIVE;
		else
			return CellState.DEAD;
	}

	@Override
	public int numberOfRows() {
		return currentBrian.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentBrian.numColumns();
	}

	@Override
	public IGrid getGrid() {
		return currentBrian;
	}

	private int countNeighbors(int row, int col) {
		int aliveNeighbours = 0;
		for (int r = (row - 1); r < (row + 2); r++) {
			for (int c = (col - 1); c < (col + 2); c++) {
				if (c < 0 || r < 0 || r >= numberOfRows() || c >= numberOfColumns())
					continue;
				if ((r != row || c != col) && getCellState(r, c) == CellState.ALIVE)
					aliveNeighbours++;
			}
		}
		return aliveNeighbours;
	}
	
}
