package datastructure;

import java.util.ArrayList;
import cellular.CellState;

public class CellGrid implements IGrid {
	private int rows;
	private int columns;
	private ArrayList<CellState> list;
	
    public CellGrid(int rows, int columns, CellState initialState) {
    	this.rows = rows;
    	this.columns = columns;
    	this.list = new ArrayList<CellState>();
    	
    	int numEntries = rows*columns;
		for (int i = 0; i < numEntries; i++) {
			list.add(initialState);
		}

	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
    	if (row > this.numRows() || column > this.numColumns() || row < 0 || column < 0) {
			throw new IndexOutOfBoundsException("Index for row " + row + " column " + column + " is out of bounds, max row is " + this.numRows() + " and max column is " + this.numColumns());
		}
    	int index = getIndex(row, column);
    	
        list.set(index, element);
    }

	@Override
    public CellState get(int row, int column) {
		if (row > this.numRows() || column > this.numColumns() || row < 0 || column < 0) {
			throw new IndexOutOfBoundsException("Index for row " + row + " column " + column + " is out of bounds, max row is " + this.numRows() + " and max column is " + this.numColumns());
		}
		int index = getIndex(row, column);
        return list.get(index);
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for (int row = 0; row < this.rows; row++) {
        	for (int col = 0; col < this.columns; col++) {
        		CellState stateCopy = get(row, col);
            	gridCopy.set(row, col, stateCopy);
    		}
		}
        return gridCopy;
    }
    
    private int getIndex(int row, int column) {
    	if(row > numRows() || row < 0)
    		throw new IllegalArgumentException("row " + row + " is out of bounds, the grid has a max height of " + numRows() + " and a minimum height of 0");
    	if(column > numColumns() || column < 0)
    		throw new IllegalArgumentException("column " + column + " is out of bounds, the grid has a max widtht of " + numColumns() + " and a minimum widtht of 0");
    	return row*this.columns + column;
	}

}
